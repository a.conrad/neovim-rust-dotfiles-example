My Neovim config after just setting up basic Rust LSP support, via [rust-tools](https://github.com/simrat39/rust-tools.nvim), shared at request of a coworker.

These files need to go into `~/.config/nvim`. You will need [packer installed](https://github.com/wbthomason/packer.nvim#quickstart) (but not configured), as well as [rust-analyzer](https://rust-analyzer.github.io/).

Then, run the following to set up the plugins: `nvim --headless -c 'PackerSync'`

After this, you should be able to just open a Rust file and get decent LSP support.
