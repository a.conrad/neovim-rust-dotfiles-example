lua require('plugins')
lua require('_rust-tools')

set number relativenumber
autocmd InsertEnter * :set norelativenumber
autocmd InsertLeave * :set relativenumber
set mouse=
